import React, {useEffect} from 'react';
import logo from '../logo.svg';
import { Routes } from '../index'
import './App.css';
import {RouteComponentProps} from "@reach/router";
import ComponentOne from "./component1/ComponentOne";
import ComponentTwo from "./component2/ComponentTwo";

export enum LocalStorageKey {
    APP_STATE = 'APP_STATE',
}

interface Props {
    path: Routes
}

const App: React.FC<Props & RouteComponentProps> = ({ path }) => {
    console.log(path);
    if (path === '/component2') {
        return (
            <ComponentTwo path={path}/>
        )
    } else {
        return (
            <ComponentOne path={path}/>
        )
    }

}

export default App;
