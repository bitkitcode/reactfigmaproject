import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './app/App';
import { Router } from '@reach/router'
import * as serviceWorker from './serviceWorker';

export type Routes = '/' | '/component1' | '/component2';

interface Props {
    path: Routes
}

const Controller: React.FC<Props> = ({ path }) => <App path={path} />

ReactDOM.render(
  <React.StrictMode>
      <Router>
          <Controller path="/" />
          <Controller path="/component1" />
          <Controller path="/component2" />
      </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
